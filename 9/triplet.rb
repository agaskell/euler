def triple(a, b)
  c = Math.sqrt(a * a + b * b)
  c % 1 == 0 ? c : false
end

def get_product
  m = 2 
  while(true)
    n = m - 1
    while(n >= 1)
      c = triple(m, n)
      sum = c + m + n if c
      if sum == 1000 
        return c * m * n
      end
      n -= 1 
    end
    m += 1
  end
end

puts get_product
