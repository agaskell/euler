def gcd(a, b)
  while b != 0
    t = b
    b = a % b
    a = t
  end
  a
end

def lcm(a, b)
  a * b / gcd(a, b)
end

l = lcm(1, 2)
for i in 3..20
  l = lcm(l, i)
end

puts l
