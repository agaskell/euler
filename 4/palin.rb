def palindrome?(n)
  s = n.to_s
  mid = s.length / 2

  palin = true
  for i in 0..mid 
    palin = false if s[i] != s[i * -1 -1]
    break if !palin
  end
  palin
end

palins = []
999.downto(1) do |i|
  999.downto(1) do |j|
    palins << i * j if palindrome?(i * j)
  end
end

puts palins.max

