multiples = Array.new

for i in 1...1000
  multiples << i if i % 3 == 0 || i % 5 == 0
end

puts multiples.inject(:+)
