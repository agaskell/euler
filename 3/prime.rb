require 'prime'

num = 600851475143 
primes = Prime.each.take_while { |p| p < Math.sqrt(num) }
largest = 0

primes.each do |p|
  while(num % p == 0)
    num /= p
    largest = p
  end
end

puts largest 
