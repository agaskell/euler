require 'prime'

sum = 0

primes = Prime.each.take_while { |p| p < 2000000 }.each { |p| sum += p }

puts sum 
